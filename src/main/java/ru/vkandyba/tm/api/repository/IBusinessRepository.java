package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    Boolean existsByIndex(String userId, Integer index);

    Boolean existsById(String userId, String id);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    E add(String userId, E entity);

    void remove(String userId, E entity);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

}
